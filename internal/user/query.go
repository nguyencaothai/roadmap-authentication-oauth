package user

import (
	"fmt"
	"time"

	"local.com/basecode/config"
)

func queryUser(userInput UserInput) (User, error) {
	var user User
	query := "select * from users where username = $1"

	errQuery := config.GetDB().QueryRow(query, userInput.Username).Scan(&user.ID, &user.Username, &user.Password, &user.Created_at)
	if errQuery != nil {
		fmt.Printf("error querying user from database\n %s", errQuery)
		return User{}, errQuery
	}

	return user, nil
}

func insertOauthUser(username, password string) (int, error) {
	var id int
	query := "insert into users(username, password, created_at) values($1, $2, $3) returning id"
	errInsert := config.GetDB().QueryRow(query, username, password, time.Now()).Scan(&id)
	if errInsert != nil {
		fmt.Printf("error inserting new oauth user to database\n %s", errInsert)
		return -1, errInsert
	}

	return id, nil
}
