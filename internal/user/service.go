package user

import (
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"
	"local.com/basecode/config"
)

func generateOauthState() (string, error) {
	state := make([]byte, 16)

	if _, errRead := rand.Read(state); errRead != nil {
		fmt.Printf("error generating oauth state\n %s", errRead)
		return "", errRead
	}

	return base64.URLEncoding.EncodeToString(state), nil
}

func handleOauthCallback(code string) (string, error) {
	oauth2Token, errExChange := config.GetOauthConfig().Exchange(context.Background(), code)
	if errExChange != nil {
		fmt.Printf("error exchange code to oauth provider\n %s", errExChange)
		return "", errors.New("internal error")
	}

	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		return "", errors.New("internal error")
	}

	idToken, errVerify := config.GetVerifier().Verify(context.Background(), rawIDToken)
	if errVerify != nil {
		fmt.Printf("error verifying id_token\n %s", errVerify)
		return "", errors.New("internal error")
	}

	var claims struct {
		Sub   string `json:"sub"`
		Email string `json:"email"`
	}

	if errExtract := idToken.Claims(&claims); errExtract != nil {
		fmt.Printf("error extracting email from id_token\n %s", errExtract)
		return "", errors.New("internal error")
	}

	userID, errCheck := checkOauthUser(UserInput{
		Username: claims.Email,
		Password: claims.Sub,
	})

	if errCheck != nil {
		return "", errors.New("internal error")
	}

	token, errorGenerate := generateJWT(userID)
	if errorGenerate != nil {
		return "", errors.New("internal error")
	}

	return token, nil

}

func generateJWT(userID int) (string, error) {
	secretKey := []byte(config.GetEnv("JWT_KEY"))
	claims := JWTClaims{
		userID,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(1 * time.Hour)),
		},
	}

	jwtInstance := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	jwt, errGenerate := jwtInstance.SignedString(secretKey)
	if errGenerate != nil {
		fmt.Printf("error generating jwt\n %s", errGenerate)
		return "", errGenerate
	}

	return jwt, nil
}

func checkOauthUser(userInput UserInput) (int, error) {
	user, errQuery := queryUser(userInput)
	if errQuery != nil {
		if errQuery == sql.ErrNoRows {
			id, errInsert := insertOauthUser(userInput.Username, hashPassword(userInput.Password))
			if errInsert != nil {
				return -1, errors.New("internal error")
			}
			return id, nil
		}
		return -1, errors.New("internal error")
	}

	return user.ID, nil

}

func hashPassword(passwordInput string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(passwordInput), 14)

	return string(bytes)
}
