package user

import "github.com/golang-jwt/jwt/v5"

type UserInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type User struct {
	ID         int    `json:"userID"`
	Username   string `json:"username"`
	Password   string `json:"password"`
	Created_at string `json:"created_at"`
}

type JWTClaims struct {
	UserID int `json:"userID"`
	jwt.RegisteredClaims
}

type Token struct {
	Token string `json:"token"`
}
