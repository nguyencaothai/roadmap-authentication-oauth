package user

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"local.com/basecode/config"
)

func LoginFunc(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Login Function")
}

func OauthLoginFunc(w http.ResponseWriter, r *http.Request) {
	state, errGenerate := generateOauthState()
	if errGenerate != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	url := config.GetOauthConfig().AuthCodeURL(state)

	http.SetCookie(w, &http.Cookie{
		Name:     "state",
		Value:    state,
		Expires:  time.Now().Add(5 * time.Minute),
		Path:     "/",
		HttpOnly: true,
		Secure:   false,
	})

	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func CallbackHandleFunc(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")
	state := r.URL.Query().Get("state")

	stateCookie, errCookie := r.Cookie("state")
	if errCookie != nil {
		fmt.Printf("error no cookie state from user requests\n %s", errCookie)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	if state != stateCookie.Value {
		fmt.Printf("error csrf attack")
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	token, errHandle := handleOauthCallback(code)
	if errHandle != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	errEncode := json.NewEncoder(w).Encode(Token{Token: token})
	if errEncode != nil {
		fmt.Printf("error encoding token before returning client")
	}

}
