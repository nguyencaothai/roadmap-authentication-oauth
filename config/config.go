package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

func LoadEnv() error {
	err := godotenv.Load("config/.env")
	if err != nil {
		return fmt.Errorf("error loading .env file")
	}
	return nil
}

func GetEnv(key string) string {
	return os.Getenv(key)
}
