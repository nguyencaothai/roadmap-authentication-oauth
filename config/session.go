package config

import (
	"crypto/rand"
	"fmt"

	"github.com/gorilla/sessions"
)

var (
	sessionStore *sessions.CookieStore
)

func SessionInit() {
	sessionStore = sessions.NewCookieStore(
		generateKey(32),
		generateKey(32),
	)
}

func generateKey(length int) []byte {
	bytes := make([]byte, length)
	if _, errRead := rand.Read(bytes); errRead != nil {
		fmt.Printf("error generating key\n %s", errRead)
		return []byte{}
	}

	return bytes
}

func GetStore() *sessions.CookieStore {
	return sessionStore
}
