package config

import (
	"context"
	"errors"
	"fmt"

	"github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	oauth2Config *oauth2.Config
	oidcProvider *oidc.Provider
	verifier     *oidc.IDTokenVerifier
)

func OauthInit() error {
	oauth2Config = &oauth2.Config{
		RedirectURL:  GetEnv("REDIRECT_URL"),
		ClientID:     GetEnv("CLIENTID"),
		ClientSecret: GetEnv("CLIENTSECRET"),
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
		},
		Endpoint: google.Endpoint,
	}

	oidcProvider, errProvider := oidc.NewProvider(context.Background(), "https://accounts.google.com")
	if errProvider != nil {
		fmt.Printf("error creating a new provider oauth\n %s", errProvider)
		return errors.New("error creating provider oauth")
	}

	verifier = oidcProvider.Verifier(&oidc.Config{ClientID: oauth2Config.ClientID})

	return nil
}

func GetOidcProvider() *oidc.Provider {
	return oidcProvider
}

func GetVerifier() *oidc.IDTokenVerifier {
	return verifier
}

func GetOauthConfig() *oauth2.Config {
	return oauth2Config
}
