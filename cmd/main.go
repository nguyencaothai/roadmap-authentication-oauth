package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"local.com/basecode/config"
	"local.com/basecode/internal/user"
)

func main() {
	errLoadEnv := config.LoadEnv()
	if errLoadEnv != nil {
		fmt.Println(errLoadEnv)
	}

	errDatabaseInit := config.DatabaseInit()
	if errDatabaseInit != nil {
		fmt.Println(errDatabaseInit)
	}

	errMigrate := config.RunMigration()
	if errMigrate != nil {
		fmt.Println(errMigrate)
	}

	errOauthInit := config.OauthInit()
	if errOauthInit != nil {
		fmt.Println(errOauthInit)
	}

	r := mux.NewRouter()

	userRoute := r.PathPrefix("/users").Subrouter()
	// userRoute.Use(authentication.AuthMiddlware)
	userRoute.HandleFunc("/login", user.LoginFunc).Methods("POST")

	oauthRoute := userRoute.PathPrefix("/oauth").Subrouter()
	oauthRoute.HandleFunc("/login", user.OauthLoginFunc).Methods("GET")
	oauthRoute.HandleFunc("/callback", user.CallbackHandleFunc).Methods("GET")

	errStart := http.ListenAndServe(":8080", r)
	if errStart != nil {
		fmt.Printf("error starting server at port 8080\n %s", errStart)
	}

}
